//
//  PixeloError.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

public struct PixeloError: Error {
    
    // MARK: - Properties
    
    let kind: PixeloErrorKind
    let message: String
    
    /// Retrieve the localized description for this error
    public var localizedDescription: String {
        return "[\(kind.description)] - \(message)"
    }
    
    // MARK: - Types
    
    enum PixeloErrorKind: CustomStringConvertible {
        case invalidRequest
        case jsonParseError
        case networkError
        case keychainError
        case missingClient
        case requestFailed
        
        var description: String {
            switch self {
            case .invalidRequest:
                return "invalidRequest"
            case .jsonParseError:
                return "jsonParseError"
            case .networkError:
                return "networkError"
            case .keychainError:
                return "keychainError"
            case .missingClient:
                return "missingClient"
            case .requestFailed:
                return "requestFailed"
            }
        }
    }
    
}
