//
//  KeychainController.swift
//  Pixelo
//
//  Created by Feby on 18/01/18.
//

import Security
import Foundation

open class KeychainController {

    var keyPrefix = ""

    private typealias Keychain = KeychainConstants
    
    /// Instantiate a KeychainController object
    public init() {}
    
    /**
     - parameter keyPrefix: a prefix that is added before the key in get/set methods.
     */
    public init(keyPrefix: String) {
        self.keyPrefix = keyPrefix
    }

    /**
     Stores the data in the keychain item under the given key.

     - parameter key: Key under which the data is stored in the keychain.
     - parameter value: Data to be written to the keychain.
     - parameter withAccess: Value that indicates when your app needs access to the text in the keychain item.

     - returns: True if the text was successfully written to the keychain.
     */
    @discardableResult
    open func set(_ value: String, forKey key: String) -> Bool {

        if let value = value.data(using: String.Encoding.utf8) {
            // Delete any existing key before saving it
            _ = delete(self.keyPrefix + key)
            // Initialise the query for keychain access
            let query: [String: Any] = [
                Keychain.secClass: kSecClassGenericPassword,
                Keychain.attrAccount: self.keyPrefix + key,
                Keychain.valueData: value,
                Keychain.accessible: kSecAttrAccessibleWhenUnlocked as String
            ]
            // Execute the query
            let resultCode = SecItemAdd(query as CFDictionary, nil)
            return resultCode == noErr
        }
        return false
    }

    /**
     Retrieves the data from the keychain that corresponds to the given key.

     - parameter key: The key that is used to read the keychain item.
     - returns: The text value from the keychain. Returns nil if unable to read the item.
     */
    open func get(_ key: String) -> String? {
        // Initialise the query for keychain access
        let query: [String: Any] = [
            Keychain.secClass: kSecClassGenericPassword,
            Keychain.attrAccount: self.keyPrefix + key,
            Keychain.returnData: kCFBooleanTrue,
            Keychain.matchLimit: kSecMatchLimitOne
        ]
        var result: AnyObject?
        // Execute the query
        let resultCode = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        if resultCode == noErr, let data = result as? Data, let currentString = String(data: data, encoding: .utf8) {
            // Get the keychain string
            return currentString
        }
        return nil
    }

    /**
     Deletes the single keychain item specified by the key.

     - parameter key: The key that is used to delete the keychain item.
     - returns: True if the item was successfully deleted.
     */
    open func delete(_ key: String) -> Bool {
        // Initialise the query for keychain access
        let query: [String: Any] = [
            Keychain.secClass: kSecClassGenericPassword,
            Keychain.attrAccount: self.keyPrefix + key
        ]
        // Call to delete the item
        let resultCode = SecItemDelete(query as CFDictionary)
        return resultCode == noErr
    }

}
