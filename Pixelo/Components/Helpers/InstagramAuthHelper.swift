//
//  InstagramAuthHelper.swift
//  Pixelo
//
//  Created by Feby on 16/01/18.
//

import Foundation

extension PixeloController {

    func instagramAuthUrl() -> URL? {
        let scopes = InstagramAPIConstants.Scopes.components(separatedBy: ",")
        var components = URLComponents(string: api.authUri)
        if components == nil {
            return nil
        }

        components?.queryItems = [
            URLQueryItem(name: "client_id", value: client.id),
            URLQueryItem(name: "redirect_uri", value: client.redirectUri),
            URLQueryItem(name: "response_type", value: "token"),
            URLQueryItem(name: "scope", value: scopes.joined(separator: "+"))
        ]
        return components?.url
    }

}
