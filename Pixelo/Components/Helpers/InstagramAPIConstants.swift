//
//  InstagramAPIConstants.swift
//  Pixelo
//
//  Created by Feby on 16/01/18.
//

import UIKit

struct InstagramAPIConstants {

    static let ClientId = "cbda7c6ad015455ab84279795fdf16e1"
    static let RedirectURI = "https://about.me/febysam"
    static let BaseURL = "https://api.instagram.com/v1"
    static let AuthURL = "https://api.instagram.com/oauth/authorize"
    static let Scopes = "basic,follower_list,public_content"

}
