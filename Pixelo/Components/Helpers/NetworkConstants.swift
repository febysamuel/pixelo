//
//  NetworkConstants.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

struct NetworkConstants {
    
    static let MaxConnections: Int = 3
    static let TimeOutForRequest: TimeInterval = 120
    static let TimeOutForResource: TimeInterval = 120
    
}
