//
//  InstagramMedia.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

/// The struct containing an Instagram media.

public struct InstagramMedia: Decodable {

    // MARK: - Properties

    /// The media identifier.
    public let id: String

    /// The owner of the media.
    public let user: InstagramUser

    /// The date and time when the media was created.
    public let createdDate: Date

    /// The type of media. It can be "image" or "video".
    public let type: String

    /// The thumbnail, low and standard resolution images of the media.
    public let images: Images

    /// A Count object that contains the number of likes on the media.
    public let likes: Count

    /// The URL link of the media.
    public let link: URL

    // MARK: - Types

    /// A struct cointaing the number of elements.
    public struct Count: Decodable {

        /// The number of elements.
        public let count: Int
    }

    /// A struct containing the resolution of a video or image.
    public struct Resolution: Decodable {

        /// The width of the media.
        public let width: Int

        /// The height of the media.
        public let height: Int

        /// The URL to download the media.
        public let url: URL
    }

    /// A struct cointaining the thumbnail, low and high resolution images of the media.
    public struct Images: Decodable {

        /// A Resolution object that contains the width, height and URL of the thumbnail.
        public let thumbnail: Resolution

        /// A Resolution object that contains the width, height and URL of the low resolution image.
        public let lowResolution: Resolution

        /// A Resolution object that contains the width, height and URL of the standard resolution image.
        public let standardResolution: Resolution

        private enum CodingKeys: String, CodingKey {
            case thumbnail
            case lowResolution = "low_resolution"
            case standardResolution = "standard_resolution"
        }
    }

    private enum CodingKeys: String, CodingKey {
        case id, user, type, images, likes, link
        case createdTime = "created_time"
    }

    // MARK: - Initializers

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        id = try container.decode(String.self, forKey: .id)
        user = try container.decode(InstagramUser.self, forKey: .user)
        let createdTime = try container.decode(String.self, forKey: .createdTime)
        createdDate = Date(timeIntervalSince1970: Double(createdTime)!)
        type = try container.decode(String.self, forKey: .type)
        images = try container.decode(Images.self, forKey: .images)
        likes = try container.decode(Count.self, forKey: .likes)
        link = try container.decode(URL.self, forKey: .link)
    }

}
