//
//  PixeloController.swift
//  Pexelo
//
//  Created by Feby on 15/01/18.
//

import UIKit

class PixeloController {
    
    // MARK: - Types

    public typealias AuthSuccessHandler = () -> Void
    public typealias SuccessHandler<T> = (_ data: T) -> Void
    public typealias FailureHandler = (_ error: PixeloError) -> Void

    // MARK: - Properties
        
    public var client: (id: String, redirectUri: String)
    public var api: (authUri: String, baseUri: String)
    private var keychain: (prefix: String, accessTokenKey: String)

    public var currentInstagramUser: InstagramUser? = nil
    
    public var urlSession: URLSession? = nil
    public var cache:NSCache<AnyObject, AnyObject>!
    private let keychainController = KeychainController()
    
    public let errorBannerHeight: CGFloat = 50.0

    // MARK: - Initializers

    init() {
        client = (InstagramAPIConstants.ClientId, InstagramAPIConstants.RedirectURI)
        api = (InstagramAPIConstants.AuthURL, InstagramAPIConstants.BaseURL)
        
        keychain = (InstagramKeychainConstants.Prefix, InstagramKeychainConstants.AccessTokenKey)
        keychainController.keyPrefix = keychain.prefix
        
        cache = NSCache()
        setupURLSession()
    }
    
    private func setupURLSession() {
        let configuration: URLSessionConfiguration = .default
        configuration.httpMaximumConnectionsPerHost = NetworkConstants.MaxConnections
        configuration.timeoutIntervalForRequest = NetworkConstants.TimeOutForRequest
        configuration.timeoutIntervalForResource = NetworkConstants.TimeOutForResource
        urlSession = URLSession(configuration: configuration)
    }
    
    /// Ends the current session.
    ///
    /// - returns: True if the user was successfully logged out, false otherwise.
    
    @discardableResult func logout() -> Bool {
        cancelAllSessionTasks()
        return deleteAccessToken()
    }
    
    /// Returns whether a user is currently authenticated or not.
    
    var isInstagramUserAuthenticated: Bool {
        return retrieveAccessToken() != nil
    }
    
    // MARK: - Access Token
    
    @discardableResult func storeAccessToken(_ accessToken: String) -> Bool {
        return keychainController.set(accessToken, forKey: keychain.accessTokenKey)
    }
    
    func retrieveAccessToken() -> String? {
        return keychainController.get(keychain.accessTokenKey)
    }
    
    @discardableResult func deleteAccessToken() -> Bool {
        return keychainController.delete(keychain.accessTokenKey)
    }
    
    func isSessionRunningDataTasks(completion: @escaping (Bool) -> ()) {
        urlSession?.getTasksWithCompletionHandler({ (dataTasks, _, _) in
            completion(dataTasks.isEmpty == true ? false: true)
        })
    }
    
    func cancelAllSessionTasks() {
        urlSession?.getTasksWithCompletionHandler({ (dataTasks, _, _) in
            dataTasks.forEach({ (dataTask) in
                dataTask.cancel()
            })
        })
    }
    
    func handleError(error: PixeloError) {
        cancelAllSessionTasks()
        
        if error.kind != .networkError {
            return
        }
        
        DispatchQueue.main.async {
            self.presentErrorBanner(message: error.message)
        }
    }
    
}
