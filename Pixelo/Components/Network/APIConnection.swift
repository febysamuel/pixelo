//
//  APIConnection.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

protocol APIConnectable {
    func connect(resource: APIResource,
                 urlSession: URLSession,
                 completion: @escaping(_ data: Data?, _ error: Error?) -> Void)
}

public class APIConnection {

}

extension APIConnection: APIConnectable {

    func connect(resource: APIResource,
                 urlSession: URLSession,
                 completion: @escaping(_ data: Data?, _ error: Error?) -> Void) {
        let urlRequest = resource.apiRequest()

        urlSession.dataTask(with: urlRequest) { (data, _, error) in
            completion(data, error)
            }.resume()
    }

}
