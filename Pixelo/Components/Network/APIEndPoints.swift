//
//  APIEndPoints.swift
//  Pixelo
//
//  Created by Feby on 21/01/18.
//

import Foundation

extension PixeloController {

    // MARK: - User Endpoints

    /// Get information about a user.
    ///
    /// - parameter userId: The ID of the user whose information to retrieve, or "self" to reference the currently
    ///   logged-in user.
    /// - parameter success: The callback called after a correct retrieval.
    /// - parameter failure: The callback called after an incorrect retrieval.
    ///

    public func instagramUser(_ userId: String?, success: SuccessHandler<InstagramUser>?, failure: FailureHandler?) {
        if userId == nil {
            request("/users/self", success: success, failure: failure)
            return
        }
        request("/users/\(userId ?? "")", success: success, failure: failure)
    }

    /// Get the list of users this user follows.
    ///
    /// - parameter success: The callback called after a correct retrieval.
    /// - parameter failure: The callback called after an incorrect retrieval.

    public func instagramFollows(success: SuccessHandler<[InstagramUser]>?, failure: FailureHandler?) {
        request("/users/self/follows", success: success, failure: failure)
    }

    /// Get the most recent media published by a user.
    ///
    /// - parameter userId: The ID of the user whose recent media to retrieve, or "self" to reference the currently
    ///   logged-in user.
    /// - parameter count: Count of media to return.
    /// - parameter success: The callback called after a correct retrieval.
    /// - parameter failure: The callback called after an incorrect retrieval.

    public func instagramMedia(fromUser userId: String,
                               count: Int?,
                               success: SuccessHandler<[InstagramMedia]>?,
                               failure: FailureHandler?) {
        var parameters: [String: Any]? = nil
        if count != nil {
            parameters = ["count": count!]
        }
        request("/users/\(userId)/media/recent", parameters: parameters, success: success, failure: failure)
    }

}
