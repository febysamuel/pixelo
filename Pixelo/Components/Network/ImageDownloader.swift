//
//  ImageDownloader.swift
//  Pixelo
//
//  Created by Feby on 26/01/18.
//

import Foundation

extension PixeloController {

    func downloadImage(forUrl url:URL,
                       success:@escaping (_ data: Data) -> (),
                       failure:(_ error: PixeloError) -> Void) {
        let request = URLRequest(url: url)
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            if data != nil {
                self.cache.setObject(data as AnyObject, forKey: url.absoluteString as AnyObject)
                success(data!)
            }
        }.resume()
    }

}
