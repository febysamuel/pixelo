//
//  APIRequestHandler.swift
//  Pixelo
//
//  Created by Feby on 21/01/18.
//

import Foundation

extension PixeloController {

    // MARK: -

    func request<T: Decodable>(_ endpoint: String,
                               parameters: [String: Any]? = nil,
                               success: SuccessHandler<T>?,
                               failure: FailureHandler?) {
        var params = [String: Any]()
        if parameters != nil {
            parameters?.forEach({ (key, value) in
                params[key] = value
            })
        }
        params["access_token"] = retrieveAccessToken()

        let apiResource = APIResource(baseUri: api.baseUri,
                                      endPoint: endpoint,
                                      method: .get,
                                      parameters: params,
                                      headers: nil)
        let connection = APIConnection()
        connection.connect(resource: apiResource, urlSession: urlSession!) { (data, error) in
            if let data = data {
                DispatchQueue.global(qos: .utility).async {
                    do {
                        let object = try JSONDecoder().decode(APIResponseParser<T>.self, from: data)
                        if let errorMessage = object.meta.errorMessage {
                            print(errorMessage)
                            DispatchQueue.main.async {
                                failure?(PixeloError(kind: .invalidRequest, message: errorMessage))
                            }
                        } else {
                            DispatchQueue.main.async {
                                success?(object.data!)
                            }
                        }
                    } catch {
                        DispatchQueue.main.async {
                            failure?(PixeloError(kind: .jsonParseError, message: error.localizedDescription))
                        }
                    }
                }
            }
            if error != nil {
                let code = (error! as NSError).code
                var pixeloError = PixeloError(kind: .requestFailed,
                                              message: "Server is not responding right now. Request Failed.")
                if code == -1009 || code == -1001 {
                    pixeloError = PixeloError(kind: .networkError,
                                              message: "No internet connection")
                }
                failure?(pixeloError)
                self.handleError(error: pixeloError)
            }
        }
    }

}
