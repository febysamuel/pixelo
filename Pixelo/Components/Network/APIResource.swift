//
//  APIResource.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

typealias Headers = [String : String]

struct APIResource {

    public let baseUri: String
    public let endPoint: String
    public let method: Method
    public let parameters: [String: Any]?
    var headers: Headers?

}

enum Method: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
}

extension APIResource {

    func apiRequest() -> URLRequest {
        var urlComps = URLComponents(string: baseUri + endPoint)
        var items = [URLQueryItem]()

        parameters?.forEach({ parameter in
            items.append(URLQueryItem(name: parameter.key, value: "\(parameter.value)"))
        })
        urlComps!.queryItems = items

        var request = URLRequest(url: urlComps!.url!)
        request.httpMethod = method.rawValue

        headers?.forEach({ header in
            request.setValue(header.value, forHTTPHeaderField: header.key)
        })

        return request
    }

}
