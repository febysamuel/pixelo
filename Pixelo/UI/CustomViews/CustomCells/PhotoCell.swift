//
//  PhotoCell.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

class PhotoCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.backgroundColor = .photoCellBaseColor
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
    }

}
