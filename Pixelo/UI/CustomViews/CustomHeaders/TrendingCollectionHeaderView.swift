//
//  TrendingCollectionHeaderView.swift
//  Pixelo
//
//  Created by Feby on 21/01/18.
//

import UIKit

class TrendingCollectionHeaderView: UICollectionReusableView {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var selectionIndicatorImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImageView?.layer.cornerRadius = (self.profileImageView?.frame.width)! / 2
        profileImageView?.clipsToBounds = true
        profileImageView?.backgroundColor = .photoCellBaseColor
        
        fullNameLabel.backgroundColor = UIColor.clear
        fullNameLabel.textAlignment = .left
        fullNameLabel.textColor = UIColor.darkGray
        fullNameLabel.font = UIFont.boldSystemFont(ofSize: 15.0)
        
        userNameLabel.backgroundColor = UIColor.clear
        userNameLabel.textAlignment = .left
        userNameLabel.textColor = UIColor.lightGray
        userNameLabel.font = UIFont.systemFont(ofSize: 13)
        
        selectionIndicatorImageView.backgroundColor = .clear
        selectionIndicatorImageView.image = UIImage(named: "Selection-Indicator")
    }
    
}
