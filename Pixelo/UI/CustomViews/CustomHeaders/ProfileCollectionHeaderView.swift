//
//  ProfileCollectionHeaderView.swift
//  Pixelo
//
//  Created by Feby on 25/01/18.
//

import UIKit

class ProfileCollectionHeaderView: UICollectionReusableView {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    @IBOutlet weak var postsCountLabel: UILabel!
    @IBOutlet weak var postsLabel: UILabel!
    
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followersCountLabel: UILabel!
    
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!

    @IBOutlet weak var borderLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImageView?.layer.cornerRadius = (self.profileImageView?.frame.width)! / 2
        profileImageView?.clipsToBounds = true
        profileImageView?.backgroundColor = .photoCellBaseColor
        
        fullNameLabel.backgroundColor = UIColor.clear
        fullNameLabel.textAlignment = .left
        fullNameLabel.textColor = UIColor.darkGray
        fullNameLabel.numberOfLines = 0
        fullNameLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        
        postsCountLabel.backgroundColor = UIColor.clear
        postsCountLabel.textAlignment = .center
        postsCountLabel.textColor = UIColor.darkGray
        postsCountLabel.numberOfLines = 0
        postsCountLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        postsCountLabel.text = nil
        
        postsLabel.backgroundColor = UIColor.clear
        postsLabel.textAlignment = .center
        postsLabel.textColor = UIColor.lightGray
        postsLabel.numberOfLines = 0
        postsLabel.font = UIFont.systemFont(ofSize: 15.0)
        postsLabel.text = "posts"
        
        followersCountLabel.backgroundColor = UIColor.clear
        followersCountLabel.textAlignment = .center
        followersCountLabel.textColor = UIColor.darkGray
        followersCountLabel.numberOfLines = 0
        followersCountLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        followersCountLabel.text = nil
        
        followersLabel.backgroundColor = UIColor.clear
        followersLabel.textAlignment = .center
        followersLabel.textColor = UIColor.lightGray
        followersLabel.numberOfLines = 0
        followersLabel.font = UIFont.systemFont(ofSize: 15.0)
        followersLabel.text = "followers"
        
        followingCountLabel.backgroundColor = UIColor.clear
        followingCountLabel.textAlignment = .center
        followingCountLabel.textColor = UIColor.darkGray
        followingCountLabel.numberOfLines = 0
        followingCountLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        followingCountLabel.text = nil
        
        followingLabel.backgroundColor = UIColor.clear
        followingLabel.textAlignment = .center
        followingLabel.textColor = UIColor.lightGray
        followingLabel.numberOfLines = 0
        followingLabel.font = UIFont.systemFont(ofSize: 15.0)
        followingLabel.text = "following"
        
        borderLabel.backgroundColor = .photoCellBaseColor
        borderLabel.textColor = .clear
        borderLabel.text = nil
    }
    
}
