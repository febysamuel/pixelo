//
//  ErrorBanner.swift
//  Pixelo
//
//  Created by Feby on 26/01/18.
//

import UIKit

extension PixeloController {
    
    static let animationDuration: TimeInterval = 0.3
    static let bannerPresentedDuration: TimeInterval = 3.0
    
    func presentErrorBanner(message: String) {
        if let window = UIApplication.shared.keyWindow {
            if window.viewWithTag(1167) != nil {
                return
            }
            let bannerViewFrame = self.bannerInitialFrame(overWindow: window)
            
            let maskView = self.getMaskView(inFrame: bannerViewFrame)
            window.addSubview(maskView)
            
            let bannerView = self.getBannerView(inFrame: bannerViewFrame, message: message)
            window.addSubview(bannerView)
            
            self.animateViews(maskView, bannerView, overWindow: window)
        }
    }
    
    fileprivate func getMaskView(inFrame frame: CGRect) -> UIView {
        let view: UIView = UIView(frame: frame)
        view.backgroundColor = .lightGray
        view.alpha = 0.4
        view.tag = 1167
        return view
    }
    
    fileprivate func getBannerView(inFrame frame: CGRect, message: String) -> UIView {
        let bannerView: UIView = UIView(frame: frame)
        bannerView.backgroundColor = .clear
        
        let maxWidth = frame.width
        
        let errorLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: maxWidth, height: frame.width))
        errorLabel.backgroundColor = .clear
        errorLabel.textColor = .white
        errorLabel.textAlignment = .center
        errorLabel.font = UIFont.systemFont(ofSize: 20.0)
        errorLabel.text = message
        bannerView.addSubview(errorLabel)
        
        return bannerView
    }
    
    fileprivate func animateViews(_ maskView: UIView, _ bannerView: UIView, overWindow window:UIWindow) {
        UIView.animate(withDuration: PixeloController.animationDuration, animations: {
            let bannerFrame = self.bannerLoadedFinalFrame(overWindow: window)
            maskView.frame = bannerFrame
            bannerView.frame = bannerFrame
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + PixeloController.bannerPresentedDuration) {
            UIView.animate(withDuration: PixeloController.animationDuration, animations: {
                let bannerFrame = self.bannerInitialFrame(overWindow: window)
                maskView.frame = bannerFrame
                bannerView.frame = bannerFrame
            }, completion: { (_) in
                maskView.removeFromSuperview()
                bannerView.removeFromSuperview()
            })
        }
    }
    
    fileprivate func bannerInitialFrame(overWindow window: UIWindow) -> CGRect {
        return CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: self.errorBannerHeight)
    }
    
    fileprivate func bannerLoadedFinalFrame(overWindow window: UIWindow) -> CGRect {
        return CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: self.errorBannerHeight)
    }
    
}
