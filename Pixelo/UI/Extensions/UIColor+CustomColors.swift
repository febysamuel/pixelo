//
//  UIColor+CustomColors.swift
//  Pixelo
//
//  Created by Feby on 16/01/18.
//

import UIKit

public extension UIColor {
    
    static let baseColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
    static let navigationBarTintColor = UIColor.white
    static let navigationTintColor = UIColor(red: 156/255, green: 156/255, blue: 136/255, alpha: 1.0)
    static let photoViewBaseColor = UIColor.black
    static let photoCellBaseColor = baseColor
    
}
