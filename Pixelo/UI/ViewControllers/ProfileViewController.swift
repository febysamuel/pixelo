//
//  ProfileViewController.swift
//  Pixelo
//
//  Created by Feby on 24/01/18.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileCollectionView: UICollectionView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var borderLabel: UILabel!
    
    var pixeloController: PixeloController?
    var instagramUser: InstagramUser?
    private let refreshControl = UIRefreshControl()
    
    fileprivate let reuseCellIdentifier = "PhotoCell"
    fileprivate let reuseHeaderIdentifier = "ProfileCollectionHeaderView"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupProfileView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        fetchUserInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ProfileViewController {
    
    private func setupProfileView() {
        setupNavView()
        setupRefreshControl()
        setupCollectionView()
    }
    
    private func setupNavView() {        
        backButton.setImage(UIImage(named: "Left-arrow"), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        userNameLabel.font = UIFont.boldSystemFont(ofSize: 20)
        userNameLabel.textColor = .darkGray
        userNameLabel.textAlignment = .center
        userNameLabel.text = instagramUser?.username
        
        logoutButton.setImage(UIImage(named: "Logout"), for: .normal)
        logoutButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        logoutButton.isHidden = true
        
        if pixeloController?.currentInstagramUser?.id == instagramUser?.id {
            logoutButton.isHidden = false
        }
        
        borderLabel.backgroundColor = .lightGray
        borderLabel.textColor = .clear
        borderLabel.text = nil
    }
    
    private func setupRefreshControl() {
        if #available(iOS 10.0, *) {
            profileCollectionView.refreshControl = refreshControl
        } else {
            profileCollectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshProfileView(_:)), for: .valueChanged)
        refreshControl.beginRefreshing()
    }
    
    private func setupCollectionView() {
        profileCollectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil),
                                       forCellWithReuseIdentifier: reuseCellIdentifier)
        
        profileCollectionView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil),
                                       forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                       withReuseIdentifier: reuseHeaderIdentifier)
        
        profileCollectionView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
    }
    
    @objc private func refreshProfileView(_ sender: Any) {
        fetchUserInfo()
    }
    
    private func fetchUserInfo() {
        pixeloController?.instagramUser(instagramUser?.id, success: { (user) in
            self.handleUser(user: user)
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func handleError(error: PixeloError) {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    private func handleUser(user: InstagramUser) {
        var instaUser = user
        instaUser.userMedia = instagramUser?.userMedia
        instagramUser = instaUser
        
        self.fetchMedia(forUser: user.id)
    }
    
    private func fetchMedia(forUser userId: String) {
        pixeloController?.instagramMedia(fromUser: userId, count: nil, success: { (media) in
            self.handleMedia(media: media, forUser: userId)
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func handleMedia(media: [InstagramMedia], forUser userId: String) {
        pixeloController?.isSessionRunningDataTasks(completion: { (status) in
            if status == false {
                DispatchQueue.main.async {
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        })
        
        if let userMediaAdded = instagramUser?.userMedia {
            var indicesToRemove = [Int]()
            for i in 0 ..< userMediaAdded.count {
                let userMedia = userMediaAdded[i]
                if media.index(where: { $0.id == userMedia.id }) == nil {
                    indicesToRemove.append(i)
                }
            }
            
            if indicesToRemove.isEmpty == false {
                indicesToRemove = indicesToRemove.sorted(by: >)
                
                for index in indicesToRemove {
                    instagramUser?.userMedia?.remove(at: index)
                }
            }
        }
        
        for instagramMedia in media {
            if instagramUser?.userMedia == nil {
                instagramUser?.userMedia = [InstagramMedia]()
            }
            if instagramUser?.userMedia?.index(where: { $0.id == instagramMedia.id }) == nil {
                instagramUser?.userMedia?.append(instagramMedia)
                self.downloadImage(url: instagramMedia.images.thumbnail.url)
            }
        }
        DispatchQueue.main.async {
            self.profileCollectionView.reloadData()
        }
    }
    
    private func downloadImage(url: URL) {
        if pixeloController?.cache.object(forKey: url.absoluteString as AnyObject) != nil {
            return
        }
        pixeloController?.downloadImage(forUrl: url, success: { (data) in
            DispatchQueue.main.async {
                self.profileCollectionView.reloadData()
            }
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func logoutCurrentUser() {
        pixeloController?.deleteAccessToken()
        self.dismissView()
    }
    
    private func dismissView() {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension ProfileViewController {
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func logoutButtonClicked(_ sender: UIButton) {
        let alertController = UIAlertController(title: nil,
                                                message: "Are you sure you want to logout?",
                                                preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let logoutAction = UIAlertAction(title: "Yes", style: .default) { (_) in
            self.logoutCurrentUser()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(logoutAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension ProfileViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return instagramUser?.userMedia?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell {
        let cell: PhotoCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier,
                                                                 for: indexPath) as! PhotoCell
        // Configure the cell
        if let mediaArray = instagramUser?.userMedia {
            let mediaItem = mediaArray[indexPath.item]
            if let cachedImage =
                pixeloController?.cache.object(forKey: mediaItem.images.thumbnail.url.absoluteString as AnyObject) {
                let image: UIImage = UIImage(data: (cachedImage as? Data)!)!
                cell.imageView.image = image
            }
        }
        return cell
    }
    
}

extension ProfileViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = self.view.frame.width
        let columns: CGFloat = 3
        let width = (totalWidth - (columns - 1)) / columns
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let photoVC =
            storyboard.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController {
            if let mediaArray = instagramUser?.userMedia {
                let media = mediaArray[indexPath.item]
                photoVC.pixeloController = pixeloController
                photoVC.media = media
                self.present(photoVC, animated: true, completion: nil)
            }
        }
    }
    
}

extension ProfileViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil
        
        if kind == UICollectionElementKindSectionHeader {
            if let headerView: ProfileCollectionHeaderView =
                collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                withReuseIdentifier: reuseHeaderIdentifier,
                                                                for: indexPath) as? ProfileCollectionHeaderView {
                if let cachedImage =
                    pixeloController?.cache.object(forKey: instagramUser?.profilePicture.absoluteString as AnyObject) {
                    let image: UIImage = UIImage(data: (cachedImage as? Data)!)!
                    headerView.profileImageView.image = image
                }
                headerView.fullNameLabel.text = instagramUser?.fullName
                
                let posts: NSNumber = NSNumber(value: instagramUser?.counts?.media ?? 0)
                headerView.postsCountLabel.text = posts.stringValue
                
                let followers: NSNumber = NSNumber(value: instagramUser?.counts?.followedBy ?? 0)
                headerView.followersCountLabel.text = followers.stringValue
                
                let following: NSNumber = NSNumber(value: instagramUser?.counts?.follows ?? 0)
                headerView.followingCountLabel.text = following.stringValue
               
                reusableView = headerView
            }
        }
        
        return reusableView!
    }
}
