//
//  InstagramLoginViewController.swift
//  Pixelo
//
//  Created by Feby on 16/01/18.
//

import UIKit
import WebKit

class InstagramLoginViewController: UIViewController {
    
    // MARK: - Properties

    @IBOutlet weak var progressView: UIProgressView!
    private var loginWebView: WKWebView? = nil
    var pixeloController: PixeloController?

    private var webView: WKWebView!
    private var webViewObservation: NSKeyValueObservation!
    private var cancelButton: UIButton!

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupLoginView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        progressView.alpha = 1.0
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        progressView.removeFromSuperview()
        webViewObservation.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension InstagramLoginViewController {
    
    private func setupLoginView() {
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        // Initializes progress view
        setupProgressView()
        
        // Initializes web view
        loginWebView = setupWebView()
        
        guard let authUrl = pixeloController?.instagramAuthUrl() else {
            //handle error
            return
        }
        
        // Starts authorization
        loginWebView?.load(URLRequest(url: authUrl, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData))
        
        setupCancelButton()
    }
    
    private func setupProgressView() {
        progressView.progress = 0.0
        progressView.tintColor = UIColor(red: 0.88, green: 0.19, blue: 0.42, alpha: 1.0)
        progressView.translatesAutoresizingMaskIntoConstraints = false
        progressView.alpha = 0.0
    }
    
    private func setupWebView() -> WKWebView {
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = .nonPersistent()
        
        webView = WKWebView(frame: CGRect(x: 0, y: 22, width: view.frame.width, height: view.frame.height - 22), configuration: webConfiguration)
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        webView.navigationDelegate = self
        
        webViewObservation = webView.observe(\.estimatedProgress, changeHandler: progressViewChangeHandler)
        
        view.addSubview(webView)

        return webView
    }
    
    private func setupCancelButton() {
        cancelButton = UIButton(type: .custom)
        cancelButton.frame = CGRect(x: 3, y: 4.5, width: 36, height: 36)
        cancelButton.backgroundColor = .clear
        cancelButton.setImage(UIImage(named: "Dismiss-Blue"), for: .normal)
        cancelButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        cancelButton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        loginWebView?.scrollView.addSubview(cancelButton)
    }
    
    private func progressViewChangeHandler<Value>(webView: WKWebView, change: NSKeyValueObservedChange<Value>) {
        progressView.setProgress(Float(webView.estimatedProgress), animated: true)
        
        if webView.estimatedProgress >= 1.0 {
            UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: {
                self.progressView.alpha = 0.0
                self.cancelButton.setImage(UIImage(named: "Dismiss-White"), for: .normal)
                self.webView.frame = CGRect(x: 0, y: 20, width: self.view.frame.width, height: self.view.frame.height - 20)
            }, completion: { (_ finished) in
                self.progressView.progress = 0
            })
        }
    }
    
    @objc private func dismissView() {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: - WKNavigationDelegate

extension InstagramLoginViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        navigationItem.title = webView.title
    }

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let urlString = navigationAction.request.url!.absoluteString
        guard let range = urlString.range(of: "#access_token=") else {
            decisionHandler(.allow)
            return
        }

        decisionHandler(.cancel)

        DispatchQueue.main.async {
            self.pixeloController?.storeAccessToken(String(urlString[range.upperBound...]))
            self.dismissView()
        }
    }

    func webView(_ webView: WKWebView,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        guard let httpResponse = navigationResponse.response as? HTTPURLResponse else {
            decisionHandler(.allow)
            return
        }

        switch httpResponse.statusCode {
        case 400:
            decisionHandler(.cancel)
            DispatchQueue.main.async {
                // handle failure kind, .invalidRequest
            }
        default:
            decisionHandler(.allow)
        }
    }

}
