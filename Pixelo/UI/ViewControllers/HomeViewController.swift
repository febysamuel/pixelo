//
//  HomeViewController.swift
//  Pixelo
//
//  Created by Feby on 15/01/18.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    
    var pixeloController: PixeloController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupHomeView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        checkInstagramAuth()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LoadLoginSegue",
            let loginViewController = segue.destination as? InstagramLoginViewController {
            loginViewController.pixeloController = pixeloController
        }
    }

}

extension HomeViewController {
    
    private func setupHomeView() {
        logoImageView.image = UIImage(named: "Pixelo-logo")
        loginButton.setBackgroundImage(UIImage(named: "Login-Instagram"), for: .normal)
    }
    
    private func checkInstagramAuth() {
        if pixeloController?.isInstagramUserAuthenticated == true {
            loadTrendingView()
        }
    }
    
    private func loadTrendingView() {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let trendingVC = storyboard.instantiateViewController(withIdentifier: "TrendingViewController") as? TrendingViewController {
            trendingVC.pixeloController = pixeloController
            self.navigationController?.pushViewController(trendingVC, animated: false)
        }
    }
    
}
