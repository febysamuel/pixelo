//
//  PhotoViewController.swift
//  Pixelo
//
//  Created by Feby on 22/01/18.
//

import UIKit

class PhotoViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    
    var pixeloController: PixeloController?
    var media: InstagramMedia?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupPhotoView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        downloadStandardResolutionImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension PhotoViewController {
    
    private func setupPhotoView() {
        view.backgroundColor = .photoViewBaseColor
        
        dismissButton.setImage(UIImage(named: "Dismiss-White"), for: .normal)
        dismissButton.imageEdgeInsets = UIEdgeInsets(top: 6, left: 6, bottom: 6, right: 6)
        
        photoImageView.contentMode = .scaleAspectFit
        
        let url = media?.images.thumbnail.url
        if pixeloController?.cache.object(forKey: url?.absoluteString as AnyObject) != nil {
            let cachedImageData = pixeloController?.cache.object(forKey: url?.absoluteString as AnyObject)
            let image: UIImage = UIImage(data: (cachedImageData as? Data)!)!
            self.photoImageView.image = image
        }
    }
    
    private func downloadStandardResolutionImage() {
        let url = media?.images.standardResolution.url
        let request = URLRequest(url: url!)
        URLSession.shared.dataTask(with: request) { (data, _, _) in
            if data == nil {
                return
            }
            
            self.pixeloController?.cache.setObject(data as AnyObject, forKey: url?.absoluteString as AnyObject)
            
            DispatchQueue.main.async {
                let image: UIImage = UIImage(data: data!)!
                self.photoImageView.image = image
            }
        }.resume()
    }
    
}

extension PhotoViewController {
    
    @IBAction func dismissView(_ sender: Any) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
}
