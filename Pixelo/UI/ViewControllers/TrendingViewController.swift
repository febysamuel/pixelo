//
//  TrendingViewController.swift
//  Pixelo
//
//  Created by Feby on 20/01/18.
//

import UIKit

class TrendingViewController: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var trendingCollectionView: UICollectionView!
    @IBOutlet weak var borderLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()
    
    var pixeloController: PixeloController?
    var instagramFriends = [InstagramUser]()
    
    fileprivate let reuseCellIdentifier = "PhotoCell"
    fileprivate let reuseHeaderIdentifier = "TrendingCollectionHeaderView"
    fileprivate let mediaCountToDownload = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTrendingView()
        fetchUserInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension TrendingViewController {
    
    private func setupTrendingView() {
        setupNavView()
        setupRefreshControl()
        setupCollectionView()
    }
    
    private func setupNavView() {
        logoImageView.image = UIImage(named: "Pixelo-logo")
        
        borderLabel.backgroundColor = .lightGray
        borderLabel.textColor = .clear
        borderLabel.text = nil
    }
    
    private func setupRefreshControl() {
        if #available(iOS 10.0, *) {
            trendingCollectionView.refreshControl = refreshControl
        } else {
            trendingCollectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshTrending(_:)), for: .valueChanged)
        refreshControl.beginRefreshing()
    }
    
    private func setupCollectionView() {
        trendingCollectionView.register(UINib(nibName: reuseCellIdentifier, bundle: nil),
                                        forCellWithReuseIdentifier: reuseCellIdentifier)
        
        trendingCollectionView.register(UINib(nibName: reuseHeaderIdentifier, bundle: nil),
                                        forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                        withReuseIdentifier: reuseHeaderIdentifier)
        
        trendingCollectionView.contentOffset = CGPoint(x: 0, y: -refreshControl.frame.height)
    }
    
    @objc private func refreshTrending(_ sender: Any) {
        fetchUserInfo()
    }
    
    private func fetchUserInfo() {
        pixeloController?.instagramUser(nil, success: { (user) in
            self.pixeloController?.currentInstagramUser = user
            self.handleUser(user: user)
            self.fetchFollows()
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func fetchFollows() {
        pixeloController?.instagramFollows(success: { (instagramUsers) in
            instagramUsers.forEach({ (user) in
                self.handleUser(user: user)
            })
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func handleError(error: PixeloError) {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    private func fetchMedia(forUser userId: String) {
        pixeloController?.instagramMedia(fromUser: userId, count: mediaCountToDownload, success: { (media) in
            self.handleMedia(media: media, forUser: userId)
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func downloadImage(url: URL) {
        if pixeloController?.cache.object(forKey: url.absoluteString as AnyObject) != nil {
            return
        }
        pixeloController?.downloadImage(forUrl: url, success: { (data) in
            DispatchQueue.main.async {
                self.trendingCollectionView.reloadData()
            }
        }, failure: { (error) in
            self.handleError(error: error)
        })
    }
    
    private func handleUser(user: InstagramUser) {
        if self.instagramFriends.index(where: { $0.id == user.id }) == nil {
            self.instagramFriends.append(user)
        }
        self.downloadImage(url: user.profilePicture)
        self.fetchMedia(forUser: user.id)
        
        DispatchQueue.main.async {
            self.trendingCollectionView.reloadData()
        }
    }
    
    private func handleMedia(media: [InstagramMedia], forUser userId: String) {
        pixeloController?.isSessionRunningDataTasks(completion: { (status) in
            if status == false {
                DispatchQueue.main.async {
                    if self.refreshControl.isRefreshing {
                        self.refreshControl.endRefreshing()
                    }
                }
            }
        })
        
        if let index = self.instagramFriends.index(where: { $0.id == userId }) {
            var user = self.instagramFriends[index]
            
            if let userMediaAdded = user.userMedia {
                var indicesToRemove = [Int]()
                for i in 0 ..< userMediaAdded.count {
                    let userMedia = userMediaAdded[i]
                    if media.index(where: { $0.id == userMedia.id }) == nil {
                        indicesToRemove.append(i)
                    }
                }
                
                if indicesToRemove.isEmpty == false {
                    indicesToRemove = indicesToRemove.sorted(by: >)
                    
                    for index in indicesToRemove {
                        user.userMedia?.remove(at: index)
                    }
                }
            }
            
            for instagramMedia in media {
                if user.userMedia == nil {
                    user.userMedia = [InstagramMedia]()
                }
                if user.userMedia?.index(where: { $0.id == instagramMedia.id }) == nil {
                    user.userMedia?.append(instagramMedia)
                    self.downloadImage(url: instagramMedia.images.thumbnail.url)
                }
            }
            self.instagramFriends[index] = user
            
            DispatchQueue.main.async {
                self.trendingCollectionView.reloadData()
            }
        }
    }
    
    @objc private func loadProfile(_ gestureRecognizer: UITapGestureRecognizer) {
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .ended {
            let sectionIndex = gestureRecognizer.view?.tag
            let instagramUser = instagramFriends[sectionIndex!]

            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if let profileVC = storyboard.instantiateViewController(withIdentifier: "ProfileViewController")
                as? ProfileViewController {
                profileVC.pixeloController = pixeloController
                profileVC.instagramUser = instagramUser
                self.navigationController?.pushViewController(profileVC, animated: true)
            }
        }
    }
    
}

extension TrendingViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return instagramFriends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if instagramFriends.isEmpty == false {
            let friend = instagramFriends[section]
            if let count = friend.userMedia?.count {
                return count >= mediaCountToDownload ? mediaCountToDownload: count
            }
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) ->
        UICollectionViewCell {
        let cell: PhotoCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier,
                                                                                 for: indexPath) as! PhotoCell
        // Configure the cell
        let user = instagramFriends[indexPath.section]
        if user.userMedia != nil {
            let media = user.userMedia![indexPath.item]
            if let cachedImage =
                pixeloController?.cache.object(forKey: media.images.thumbnail.url.absoluteString as AnyObject) {
                let image: UIImage = UIImage(data: (cachedImage as? Data)!)!
                cell.imageView.image = image
            }
        }
        return cell
    }
    
}

extension TrendingViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 56)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalWidth = self.view.frame.width
        let columns: CGFloat = 3
        let width = (totalWidth - (columns - 1)) / columns
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let photoVC =
            storyboard.instantiateViewController(withIdentifier: "PhotoViewController") as? PhotoViewController {
            let user = instagramFriends[indexPath.section]
            let userMedia = user.userMedia![indexPath.item]
            
            photoVC.pixeloController = pixeloController
            photoVC.media = userMedia
            self.present(photoVC, animated: true, completion: nil)
        }
    }
    
}

extension TrendingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        var reusableView: UICollectionReusableView? = nil
        
        if kind == UICollectionElementKindSectionHeader {
            let headerView: TrendingCollectionHeaderView =
                collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader,
                                                                withReuseIdentifier: reuseHeaderIdentifier,
                                                                for: indexPath) as! TrendingCollectionHeaderView
            if instagramFriends.isEmpty == false {
                let instagramUser = instagramFriends[indexPath.section]
                if let cachedImage =
                    pixeloController?.cache.object(forKey: instagramUser.profilePicture.absoluteString as AnyObject) {
                    let image: UIImage = UIImage(data: (cachedImage as? Data)!)!
                    headerView.profileImageView.image = image
                }
                headerView.fullNameLabel.text = instagramUser.fullName
                headerView.userNameLabel.text = "@\(instagramUser.username)"
                headerView.tag = indexPath.section
                
                let tapUser: UITapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                                             action: #selector(loadProfile(_:)))
                headerView.addGestureRecognizer(tapUser)
            }
            reusableView = headerView
        }
        
        return reusableView!
    }
}
